---

title: Contributing to the FOSS Governance Collection
layout: single
permalink: /contribute/

---

## Code of Conduct

All activities related to the FOSS Governance project must adhere to our [Code of Conduct](/coc/). This includes issues, the IRC channel, and any other discussions or interactions.

## Licensing

All contributions must be under the [same license](https://gitlab.com/fossgovernance/fossgovernance/-/blob/master/LICENSE) as the FOSS Governance project.

## Issue-Driven project

The project relies on [GitLab issues](https://gitlab.com/fossgovernance/fossgovernance/-/issues) for most project management and communication.

An issue is required for any work on the project, and issues are where the canonical conversations about potential or planned changes occur. This ensures that everyone can be on the same page and the greater community has the ability to see the conversation. It also means that these conversations remain available throughout the history of the project.

Basically…

* Before doing any work on the project, [open or comment on an issue](https://gitlab.com/fossgovernance/fossgovernance/-/issues).
* Before sending a merge request, [open or comment on an issue](https://gitlab.com/fossgovernance/fossgovernance/-/issues)
* When in doubt, [open or comment on an issue](https://gitlab.com/fossgovernance/fossgovernance/-/issues).
* When not in doubt, [open or comment on an issue](https://gitlab.com/fossgovernance/fossgovernance/-/issues).

**THE ONE EXCEPTION TO THIS RULE** is anything relating to violation or enforcement of the [Code of Conduct](/coc/). Because of the sensitive nature of many of these conversations, we do not want this information in issues. Please follow the reporting directions in the [Code of Conduct](/coc/) instead.

## Ways to contribute

Right now there are four primary ways to contribute to the project:

1. Suggest a document to add to the collection
1. Report a problem with the collection, project, or website
1. Send a merge request to resolve an issue with the website
1. Ask questions or provide feedback about the project

## Sharing your knowledge

Right now the best way to contribute is to open issues to suggest new additions to the collection or to provide feedback.

1. [Start a new issue](https://gitlab.com/fossgovernance/fossgovernance/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).
1. Choose a template for the issue. You can select from:
   * Question or other
   * Report a problem
   * Suggest a document
1. Fill in the blanks to the best of your knowledge.

🎉 Voila! You've just contributed! 🎉

## Talk to us!

Want to chat? We can do that! Drop by the `#fossgovernance` channel on freenode IRC. 

Don't have an IRC client set up? That's OK! freenode provides a handy [webchat](https://kiwiirc.com/nextclient/#irc://irc.freenode.net/#fossgovernance).

Please note that, like the rest of the project, all conversations on the IRC channel fall under our [Code of Conduct](/coc/).
