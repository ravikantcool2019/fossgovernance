---
layout: single
permalink: /about/
---

Welcome to the FOSS Governance Collection! 

## The What

The goal of the project is to provide a governance reference resource for all FOSS community members. We're doing this by collecting and cataloging documents that are in some way related to the governance of FOSS projects. 

These documents live in Zotero, not in this repository. Visit [the document collection](https://www.zotero.org/groups/2310183/foss_governance/items) to start browsing and searching the collection.

## The How

1. Visit [the document collection](https://www.zotero.org/groups/2310183/foss_governance/items).
1. Browse or search the collection.
1. Alternatively, select one or more tags from the lower left to see all documents associated with those tags.

## The Why

FOSS governance is poorly defined and poorly understood by most projects. When projects do consider governance matters, they typically want to learn by example and/or fork the governance documents of another project. Unfortunately, it can be difficult to locate these documents, or at least to find documents that might suit the project's needs.

Along with providing a resource for project leaders and maintainers, this collection is intended as a research tool for people wishing to learn more about governance in free and open source software projects. Rather than scouring the web for governance-related documents, the collection provides a one-stop-shop of all things FOSS governance, in a catalogued and searchable format.

## Code of Conduct

All activities around the FOSS Governance project fall under our [Code of Conduct](/coc/). We encourage everyone to read it, since you will be held to the expectations that it sets forth.

## Contributing

Do you want to contribute to the FOSS Governance project? [We have a document for that!](/contribute/)

## Zotero

The document collection lives on [Zotero](http://zotero.org). It's a powerful and robust open source citation manager. We chose Zotero for several reasons:

* It's [free software](https://github.com/zotero).
* Its browser plugins make it very easy to gather documents.
* It can take snapshots of documents, allowing for offline access via the [Zotero Desktop App](https://www.zotero.org/download/).
* Because it was originally designed for research, it has excellent tagging and searching functionality. 
* It has basic access control which will allow for collaborators rather than having a single person be a bottleneck.
* It's searchable across the entire collection, including snapshots.
* Its tagging and metadata handling makes it easy to catalogue and locate what you need.

## Repository

The home base for this project is its [GitLab respository](https://gitlab.com/fossgovernance/fossgovernance/).

If all of the content is in Zotero, why do we even need a repository?

This repository is here for three reasons:

1. Lower the barrier to entry for contributing. Rather than requiring everyone set up Zotero to contribute, which also would require updating the Zotero access list, people can simply open an issue here.
1. This website is a static site, the files for which are in this repository.
1. Provide a home for our own governance- and organisational-related documents. Speaking of which…

## Talk to us!

Want to chat? We can do that! Drop by the `#fossgovernance` channel on freenode IRC. 

Don't have an IRC client set up? That's OK! freenode provides a handy [webchat](https://kiwiirc.com/nextclient/#irc://irc.freenode.net/#fossgovernance).

Please note that, like the rest of the project, all conversations on the IRC channel fall under our [Code of Conduct](/coc/).

## License

This project is under the [Creative Commons Attribution 4.0 International license](https://gitlab.com/fossgovernance/fossgovernance/-/blob/master/LICENSE). All contributions to the project must be submitted under the same license.
